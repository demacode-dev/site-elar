jQuery(document).ready(function ($) {

    var $form = $('form[data-hibrido-subscribers-form]');
    var $response = $('p[data-hibrido-subscribers-response]');

    if ( ! $form.length) {
        return;
    }

    var $button = $form.find('button[type="submit"]');
    var originalButtonText = $button.text();
    $form.ajaxForm({
        data: {
            action: hibrido_subscribers_ajax_object.action
        },
        dataType: 'json',
        beforeSend: function () {
            //$button.text(hibrido_subscribers_ajax_object.loadingButtonText).attr('disabled', '');
            $button.text('Enviando...').attr('disabled', '');
        },
        complete: function () {
            //$button.text(originalButtonText).removeAttr('disabled');
            $button.text("Enviado!").removeAttr('disabled');
        },
        success: function (response) {
            
            if (response.success) {
                $response.text("Email cadastrado com sucesso");
            } else {
                $response.text("Não foi possivel cadastrar o email");
            }

        }
    });

});
