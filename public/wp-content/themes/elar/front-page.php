<?php

	wp_enqueue_style('css_home', get_stylesheet_directory_uri().'/src/css/front-page.min.css', array(), null, false);
	get_header();
?>


<div class="banner-pai">
    <div class="banner-carrosel">

        <?php 
            $slide = get_field('bloco_home_inicial')['home_banner_carrossel'];
            $count = is_countable($slide) ? count($slide) : 0;

            for ($i=0; $i < $count; $i++): 
                $slide_imagem = $slide[$i]['imagem_carrossel']['url'];
                $titulo = $slide[$i]['titulo_carrossel'];
                $descricao = $slide[$i]['descricao_carrossel'];        
        ?>
            <div class="banner-background camada-verde" style="background-image: url('<?= $slide_imagem ?>')">
                <div class="banner-textos-pai centralizar">
                    <div class="banner-textos-filho">
                        <div class="banner-titulo centralizar">
                            <h1><?= $titulo ?></h1>
                        </div>
                        <div class="banner-descricao centralizar">
                            <p><?= $descricao ?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            endfor
        ?>
    </div>  
</div>
 
<div class="banner-container-pai">
    <div class="banner-conteudo">
            
        <div class="banner-arrows">
            <div class="arrow-esquerda">
                <img src="<?= get_stylesheet_directory_uri().'/img/right-arrow.svg' ?>">
            </div>
            <div class="arrow-direita">
                <img src="<?= get_stylesheet_directory_uri().'/img/right-arrow.svg' ?>">
            </div>
        </div>

    </div>
</div>

<div class="seta-ultima-banner">   
    <div class="container-total">
        <div class="container-imagem">
            <img src="<?= get_stylesheet_directory_uri().'/img/arrows_down.svg' ?>">
        </div>
    </div>   
</div>

<?php

    $background = get_field('bloco_home_informacoes')['background_card']['url'];

    $card1_icone = get_field('bloco_home_informacoes')['card_primeiro']['logo']['url'];
    $card1_titulo = get_field('bloco_home_informacoes')['card_primeiro']['titulo'];
    $card1_descricao = get_field('bloco_home_informacoes')['card_primeiro']['descricao'];

    $card2_icone = get_field('bloco_home_informacoes')['card_segundo']['logo']['url'];
    $card2_titulo = get_field('bloco_home_informacoes')['card_segundo']['titulo'];
    $card2_descricao = get_field('bloco_home_informacoes')['card_segundo']['descricao'];

    $card3_icone = get_field('bloco_home_informacoes')['card_terceiro']['logo']['url'];
    $card3_titulo = get_field('bloco_home_informacoes')['card_terceiro']['titulo'];
    $card3_descricao = get_field('bloco_home_informacoes')['card_terceiro']['descricao'];

    $card4_icone = get_field('bloco_home_informacoes')['card_quarto']['logo']['url'];
    $card4_descricao = get_field('bloco_home_informacoes')['card_quarto']['descricao'];

?>

<div class="sessao-informacoes camada-verde" style="background-image: url('<?= $background ?>')">
    <div class="container-padrao">
        <div class="informacoes-conteudo-pai centralizar">

            <div class="card-informacao-pai anime anime-left">        
                <div class="card-informacao-filho">
                    <div class="card-icone">
                        <img src="<?= $card1_icone ?>">
                    </div>
                    <div class="card-textos">
                        <div class="card-titulo">
                            <h1><?= $card1_titulo ?></h1>
                        </div>
                        <div class="card-descricao">
                            <p><?= $card1_descricao ?></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-informacao-pai anime anime-left">  
                <div class="card-informacao-filho">
                    <div class="card-icone">
                        <img src="<?= $card2_icone ?>">
                    </div>
                    <div class="card-textos">
                        <div class="card-titulo">
                            <h1><?= $card2_titulo ?></h1>
                        </div>
                        <div class="card-descricao">
                            <p><?= $card2_descricao?></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-informacao-pai anime anime-left"> 
                <div class="card-informacao-filho">
                    <div class="card-icone">
                        <img src="<?= $card3_icone ?>">
                    </div>
                    <div class="card-textos">
                        <div class="card-titulo">
                            <h1><?= $card3_titulo ?></h1>
                        </div>
                        <div class="card-descricao">
                            <p><?= $card3_descricao ?></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-informacao-pai anime anime-left"> 
                <div class="card-informacao-filho">
                    <div class="card-icone">
                        <img src="<?= $card4_icone ?>">
                    </div>
                    <div class="card-textos">
                        <div class="card-descricao">
                            <p><?= $card4_descricao ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="banner-obras centralizar">
    <div class="banner-obras-conteudo">
        <div class="obras-main">
            
            <?php 
                $carrossel = get_field('bloco_home_carrossel_obras');
                $count = is_countable($carrossel) ? count($carrossel) : 0;
                $total_carrossel = count($carrossel);

                for ($i=0; $i < $count; $i++): 
                    $carrossel_imagem = $carrossel[$i]['imagem_destaque_carrossel']['url'];
                    $titulo = $carrossel[$i]['titulo_carrossel'];
                    $descricao = $carrossel[$i]['descricao_carrossel'];
                    $background_color = $carrossel[$i]['cor_fundo_background'];
                    $color = $carrossel[$i]['cor_textos'];
                    $link =  $carrossel[$i]['link_botao_carrossel'];       
            ?> 

            <div class="obras-carrossel <?= $background_color?>" style="background-image: url('<?= $carrossel_imagem; ?>'); ">
                <div class="obras-carrossel-conteudo">
                    <div class="bloco-textos anime anime-right">
                        <h1 class="<?= $color ?>"><?= $titulo ?></h1>
                        <p class="<?= $color ?>"><?= $descricao ?></p>
                    </div>
                    <a href="<?= get_home_url()?>/nossas-obras/obras/<?= $link ?>">
                        <div class="bloco-seta-unica centralizar">
                            <img src="<?= get_stylesheet_directory_uri().'/img/right-arrow.svg' ?>">
                        </div> 
                    </a>      
                </div>
            </div>

            <?php 
                endfor;
            ?>


        </div>
        <div class="obras-carrosel-controles">
            <div class="sessoes-carrossel">
                <div class="numero-sessao">
                    <p id="sessao">01</p>
                </div>
                <div class="numeros-de-sessoes">
                    <p><?= "0".$total_carrossel?></p>
                </div>
            </div>
            <div class="carrosel-setas">
                <div class="seta-anterior" onclick="Subtrair()">
                    <img src="<?= get_stylesheet_directory_uri()?>/img/next-arrow.png">
                </div>
                <div class="seta-proximo" onclick="Somar()">
                    <img src="<?= get_stylesheet_directory_uri()?>/img/next-arrow.png">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="obras-ultimas">
    <div class="container-padrao">
        <div class="cabecalho anime anime-fade">
            <h1>Últimas obras Entregues</h1>
        </div>
        <div class="container-cards-obras centralizar anime anime-left">

             <?php

                $args = new WP_Query(array(
                    'post_type' => array('obras-de-marcenaria', 'obras-de-ambientacao', 'obras-de-decoracao'),
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'numberposts' => 4,
                    'posts_per_page' => 4,
                    'hide_empty' => false
                ));

                $posts = $args->posts;
                $count = is_countable($posts) ? count($posts) : 0;

                for ( $i=0; $i < $count; $i++ ) : 

                    $titulo = $posts[$i]->post_title;                    
                    $link_post = get_permalink( $posts[$i]->ID );                   
                    $id_obra = $posts[$i]->ID;
                    $background = get_field( 'bloco_banner_obra', $id_obra )['card_imagem_background']['url'];
                    $breve_texto = get_field( 'bloco_banner_obra', $id_obra )['descricao_obra'];
                    
            ?> 
            
                        <div class="card-obra">
                            <div class="imagem camada-preta" style="background-image: url('<?= $background ?>')">
                                <a href="<?= $link_post ?>">
                                    <div class="botao centralizar">
                                        <img src="<?= get_stylesheet_directory_uri()?>/img/right-arrow.svg">
                                    </div>
                                </a>
                                <div class="card-conteudo">
                                    <?php 
                                        if($posts[$i]->post_type == 'obras-de-marcenaria'){
                                            
                                    ?>
                                        <div class="etiqueta etiqueta-cinza-claro centralizar">
                                            <p>Marcenaria</p>
                                        </div>
                                        <?php }?>

                                    <?php 
                                    
                                        if($posts[$i]->post_type == 'obras-de-ambientacao'){ 
                                    
                                    ?>
                                        <div class="etiqueta etiqueta-cinza-claro centralizar">
                                            <p>Marcenaria</p>
                                        </div>

                                        <div class="etiqueta etiqueta-cinza centralizar">
                                            <p>Ambientação</p>
                                        </div>

                                        <div class="etiqueta etiqueta-verde centralizar">
                                            <p>Decoração</p>
                                        </div>

                                    <?php } ?>

                                    <?php

                                        if( $posts[$i]->post_type == 'obras-de-decoracao' ){
                                    ?>

                                        <div class="etiqueta etiqueta-verde centralizar">
                                            <p>Decoração</p>
                                        </div>

                                    <?php }?>
                                    
                                    <div class="textos">
                                        <h1><?= $titulo ?></h1>
                                        <p><?= $breve_texto ?></p>
                                    </div>
                                    
                                </div>
                            </div>
                                                      
                        </div>

            <?php
                    
                wp_reset_postdata();
                endfor;

            ?>
        </div>
    </div>
</div>

<?php 

    $frase = get_field('bloco_depoimento_cliente')['frase_cliente'];
    $nome_cliente = get_field('bloco_depoimento_cliente')['nome_cliente'];
    $ano_depoimento = get_field('bloco_depoimento_cliente')['ano_depoimento'];
    $foto_cliente = get_field('bloco_depoimento_cliente')['foto_cliente']['url'];

?>

<div class="container-clientes anime anime-fade">
    <div class="container-padrao">
        <div class="cliente-border">
            <div class="cliente-frase">
                <h1><?= $frase ?></h1>
                <div class="cliente-ultimo"> 
                    <div class="textos">
                        <p class="nome"><?= $nome_cliente?></p>
                        <p class="ano"><?= $ano_depoimento ?></p>
                    </div>
                </div>
            </div>
            <div class="imagem-cliente">
                <img src="<?= $foto_cliente ?>">
            </div>
        </div>
    </div>
    <div class="container-ultimo"></div>
</div>

<script type="text/javascript">

    var valor = 1;


    function Somar(){
        var total = <?= $total_carrossel?>;

        if (valor < total){
            valor+= 1;
            document.getElementById("sessao").innerHTML = "0" + valor;
        }else{
            if (valor == total){
                valor = 1;
                document.getElementById("sessao").innerHTML = "0" + valor;
            }
        }
    }

    function Subtrair(){
        var total = <?= $total_carrossel?>;

        if(valor >= 2){
            valor-= 1;
            document.getElementById("sessao").innerHTML = "0" + valor;
        }else{
            if(valor == 1){
                valor = total;
                document.getElementById("sessao").innerHTML = "0" + valor;
            }
        }

    }

    jQuery(document).ready(function($){

        $('.banner-carrosel').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            fade: true,
            prevArrow: $('.banner-arrows .arrow-esquerda'),
            nextArrow: $('.banner-arrows .arrow-direita')
        });

    });

    jQuery(document).ready(function($){

        $('.obras-main').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            fade: true,
            prevArrow: $('.carrosel-setas .seta-anterior'),
            nextArrow: $('.carrosel-setas .seta-proximo')
        });

    });

</script>

<?php get_footer(); ?>