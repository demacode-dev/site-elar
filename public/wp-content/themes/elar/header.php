<?php  do_action('before_wphead_after_init');?>
<!DOCTYPE html>
<html <?php language_attributes();?> class="no-js">

<head>

    <!-- Global site tag (gtag.js) - Google Analytics 
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-35969453-1"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'UA-35969453-1');
    </script>
    -->

    <?php
    	HC::init();
    ?>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0" />
    <title><?php echo wp_title();?></title>
    <!-- ESTILOS -->
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/header.min.css">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/footer.min.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/global.min.css">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/js/plugins/slick/slick.css"/>
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/js/plugins/slick/slick-theme.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <?php wp_head();?>
</head>
<body class="scroll-body scroll-block" id="body">

<div class="container-loading" id="container-loading">
	<div class="conteudo loading">
		<img id="logo" src="<?=get_stylesheet_directory_uri()?>/img/logo_elar_preto.png">
		<img id="gif" src="<?=get_stylesheet_directory_uri()?>/img/loading.gif">
	</div>
</div>
<header class="header">
	<div class="menu-lateral" id="menu-lateral">
		<div class="after-container">
			<div class="container-padrao">
				<div class="container-logo">
					<a href="/"><img src="<?= get_field('logo', 'header')['url']?>"></a>
				</div>
				<div class="container-menu">
					<img onclick="abrirMenu()" src="<?= get_stylesheet_directory_uri() ?>/img/close.svg">
				</div>
			</div>

			<div class="itens-responsivo">
				<ul>
					<?php 
						$itens_menu = get_field('itens_menu', 'header');
						$count = is_countable($itens_menu) ? count($itens_menu) : 0;
						$item_ativo = is_front_page() || is_home() ? '' : $post->post_name;

						for ($i=0; $i < $count; $i++) : 
							$slug = $itens_menu[$i]['slug'];
							$label = $itens_menu[$i]['nome'];
							$classe_ativa = '';
				
							if($slug == $item_ativo){
								$classe_ativa = 'active';
							}
					
					?>

					<li class="item <?= $classe_ativa; ?>">
						<a href="/<?= $slug; ?>">
							<?= $label; ?>
						</a>
						<div class="linha"></div>
					</li>

					<?php 
						endfor 
					?>

				</ul>
			</div>
			<div class="container-redes-pai">
				<div class="container-redes">
					<?php
						$redes = get_field('redes_sociais', 'geral');
						$contadorRedes = count($redes);
						for($i =0; $i < $contadorRedes; $i++){
							$rede = $redes[$i];
					?>
					<div class="container-rede">
						<a target="_blank" href="<?= $rede['link']?>">
							<img src="<?= $rede['icone']['url']?>">
							<p><?= $rede['nome']?></p>
						</a>
					</div>
					
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container-padrao">
		<div class="container-logo">
			<a href="/"><img src="<?= get_field('logo', 'header')['url']?>"></a>
		</div>
		<div class="container-menu">
			<img onclick="abrirMenu()" src="<?= get_stylesheet_directory_uri()?>/img/menu.svg">
		</div>
	</div>
</header>