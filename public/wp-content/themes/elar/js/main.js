var page = 2;
var url = window.origin + '/wp-admin/admin-ajax.php';
var button = jQuery('.carregar-mais');
var carrinho = [];

jQuery(document).ready(function($){
	$('#container-loading').fadeOut();
	$('#body').removeClass('scroll-block');

    $target = $('.anime'),
	animationClass = 'anime-init',
	windowHeight = $(window).height(),
	offset = windowHeight - (windowHeight / 5);
	var root = document.documentElement;
	root.className += ' js';

	function boxTop(idBox) {
		var boxOffset = $(idBox).offset().top;
		return boxOffset;
	}

	function animeScroll() {
		var documentTop = $(document).scrollTop();
		$target.each(function() {
		if (documentTop > boxTop(this) - offset) {
			$(this).addClass(animationClass);
		}
		});
	}
	animeScroll();

	$(document).scroll(function() {
		$target = $('.anime');
		animeScroll();
	});
});

function abrirMenu(){
	var menuLateral = document.getElementById('menu-lateral');

	if(!menuLateral.classList.contains('mostrar-menu')) {
		menuLateral.classList.add('mostrar-menu');
			
	} else {
		menuLateral.classList.remove('mostrar-menu');
	}
}
function abrirDropDown(id){
	dropdown = document.getElementById('container-options-' + id);
	seta = document.getElementById('seta-' + id);
	if(dropdown.classList.contains('dropdown-ativo')){
		dropdown.classList.remove('dropdown-ativo');
		seta.classList.remove('dropdown-ativo');
	}else{
		dropdown.classList.add('dropdown-ativo');
		seta.classList.add('dropdown-ativo');
	}
}

function filtroController(id) {
	url = removerParametro(id);
	parametro = '';

	checkBoxes = jQuery('.check-box-'+id+':checkbox:checked');
	console.log(checkBoxes);
	contadorCheckBoxes = checkBoxes.length;
	for(i = 0; i < contadorCheckBoxes; i++){
		if(checkBoxes[i].checked){
			if(i == (contadorCheckBoxes - 1)){
				parametro = parametro + checkBoxes[i].value;
			}else{
				parametro = parametro + checkBoxes[i].value + ',';
			}
		}
	}
	urlParametros = inserirParametro(id, parametro, url);

	window.location.href = urlParametros;
}

function inserirParametro(key, value, url) {
	_url = url;
	if(value != ''){
		_url += (_url.split('?')[1] ? '&':'?') + key + '=' + value;
	}
	return _url;
}

function removerParametro(key, sourceURL) {
	sourceURL = window.location.href;
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

function carregarMais(categoriaQuery, filtroQuery){
	jQuery.ajax({
		url: url,
		type: 'POST',
		async: true,
		data: {
			action: 'be_ajax_load_more',
			page: page,
			catquery: categoriaQuery,
			filtro: filtroQuery
		},
		beforeSend: function() {
			jQuery('.carregar-mais-texto').text('Carregando...');
		},
		success: function(response){
			console.log(response);
			if(response == 'false'){
				jQuery('.carregar-mais-texto').text('Não há mais Obras');
			}else{
				var arrayPosts = JSON.parse(response);
				var countArrayPosts = arrayPosts.length;

				for(i = 0; i < countArrayPosts; i++){
					var containerDivPrincipal = document.createElement("div");
					containerDivPrincipal.className = 'container-obra-pai';

					var containerDivSecundaria = document.createElement("div");
					containerDivSecundaria.className = 'container-obra camada-preta';
					containerDivSecundaria.style.backgroundImage = 'url(' + arrayPosts[i].imagem + ')';

					var containerDivTag = document.createElement("div");
					containerDivTag.className = 'container-tag ' + arrayPosts[i].tag;

					var containerDivTexto = document.createElement("div");
					containerDivTexto.className = 'container-texto';

					var containerDivBotao = document.createElement("div");
					containerDivBotao.className = 'container-botao';


					var titulo = document.createElement("h1");
					titulo.innerText = arrayPosts[i].titulo;

					var descricao = document.createElement("p");
					descricao.innerText = arrayPosts[i].descricao;

					var tagNome = document.createElement("p");
					tagNome.innerText = arrayPosts[i].post_type_name;

					var imagemSeta = document.createElement("img");
					imagemSeta.src = arrayPosts[i].imagem_seta;

					var link = document.createElement("a");
					link.href = arrayPosts[i].link;


					containerDivBotao.appendChild(imagemSeta);
					link.appendChild(containerDivBotao);

					containerDivTexto.appendChild(titulo);
					containerDivTexto.appendChild(descricao);

					containerDivTag.appendChild(tagNome);

					containerDivSecundaria.appendChild(containerDivTag);
					containerDivSecundaria.appendChild(containerDivTexto);
					containerDivSecundaria.appendChild(link);

					containerDivPrincipal.appendChild(containerDivSecundaria);

					jQuery('.container-obras').append(containerDivPrincipal);
				}
				jQuery('.carregar-mais-texto').text('Carregar mais');
				jQuery('.container-obras-pai').append( button );
				page = page + 1;
			}
		},
		error: function(err){
			jQuery('.carregar-mais-texto').text('Ocorreu algum erro');
		},
	});
}