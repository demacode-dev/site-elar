jQuery(document).ready(function($){

  jQuery('.data').mask('11/11/1111');
  jQuery('.horario').mask('00:00:00');
  jQuery('.data_hora').mask('00/00/0000 00:00:00');
  jQuery('.cep').mask('00000-000');
  jQuery('.quantidade').mask('000.000', {reverse: true});
  jQuery('.cpf').mask('000.000.000-00', {reverse: true});
  jQuery('.cnpj').mask('00.000.000/0000-00', {reverse: true});
  
  var FoneBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
  },
  telOptions = {
      onKeyPress: function(val, e, field, options) {
        field.mask(FoneBehavior.apply({}, arguments), options);
      }
  }
  jQuery('.telefone').mask(FoneBehavior, telOptions);
  jQuery('.celular').mask(FoneBehavior, telOptions);
  
});

var CpfCnpjBehavior = function (val) {
    return val.replace(/\D/g, '').length === 14 ? '00.000.000/0000-00' : '000.000.000-00999';
},
CpfCnpjOptions = {
    
    onKeyPress: function(val, e, field, options) {
      field.mask(CpfCnpjBehavior.apply({}, arguments), options);
    },
    
    clearIfNotMatch: true
}    
jQuery('.cpfcnpj').mask(CpfCnpjBehavior, CpfCnpjOptions);

function moedaParaNumero(valor)
{
    return isNaN(valor) == false ? parseFloat(valor) :   parseFloat(valor.replace("RjQuery","").replace(".","").replace(",","."));
}
function numeroParaMoeda(n, c, d, t)
{
  
    c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "jQuery1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}