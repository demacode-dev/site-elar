<?php
    wp_enqueue_style('css_contato', get_stylesheet_directory_uri().'/src/css/quem-somos.min.css', array(), null, false);
    get_header();
?>

<?php 

    $titulo = get_field('bloco_banner_inicial')['titulo_banner'];
    $texto = get_field('bloco_banner_inicial')['textos_banner'];
    $texto_bold = get_field('bloco_banner_inicial')['textos_bold_banner'];
    $background = get_field('bloco_banner_inicial')['imagem_background_banner']['url'];
    $video_url = get_field('bloco_banner_inicial')['link_video_youtube'];

?>

<div class="banner-background camada-verde" style="background-image: url('<?= $background ?>')">
    <div class="banner-container-pai">
        <div class="banner-container-conteudo-pai">
            <div class="container-padrao2">

                <div class="banner-titulo-pai centralizar">
                    <div class="banner-titulo-filho">
                        <h1><?= $titulo ?></h1>
                        <div class="textos">
                            <p><?= $texto ?></p>
                            <div class="bold">
                                <p><?= $texto_bold ?></p>
                            </div>
                        </div>
                    </div>
                </div>

                

            </div>
        </div>
    </div>
</div>

<div class="container-map-pai anime anime-fade">
    <div class="container-total">
        <div class="container-padrao">
            <iframe src="https://www.youtube.com/embed/<?= $video_url ?>?&autoplay=1" frameborder="0" allow="accelerometer; autoplay=1; loop=1; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>

<div class="sobre-nos centralizar">
    <div class="container-padrao">

        <?php 
            $cards = get_field('bloco_cards_sobre_nos');
            $count = is_countable($cards) ? count($cards) : 0;

            for ($i=0; $i < $count; $i++): 
                $background = $cards[$i]['imagem_background_card']['url'];
                $titulo = $cards[$i]['titulo_card'];   
                $texto = $cards[$i]['texto_card']; 
        ?>
        <div class="card-sobre-pai anime anime-fade" style="transition-delay: .<?=$i?>s">
            <div class="card-sobre-imagem">
                <div class="card-sobre-filho">
                    <div class="gradient"></div>
                    <img src="<?= $background ?>">
                </div>
                <div class="textos">
                    <div class="titulo">
                        <p><?= $titulo ?></p>
                    </div>
                        
                    <div class="texto">
                        <p><?= $texto ?></p>
                    </div>
                </div>
            </div>
        </div>
        

        <?php 
            endfor
        ?>

    </div>

</div>

<?php 

    $imagem = get_field('bloco_obras')['bloco_primeiro_obra']['imagem_destaque']['url'];
    $titulo = get_field('bloco_obras')['bloco_primeiro_obra']['titulo_obra'];
    $descricao = get_field('bloco_obras')['bloco_primeiro_obra']['descricao_obras'];
    $background_color = get_field('bloco_obras')['bloco_primeiro_obra']['cor_fundo_background'];
    $color = get_field('bloco_obras')['bloco_primeiro_obra']['cor_textos'];       
    $texto_botao = get_field('bloco_obras')['bloco_primeiro_obra']['texto_botao'];   
    $cor_botao = get_field('bloco_obras')['bloco_primeiro_obra']['cor_botao'];
    
?> 
<div class="obra-primeiro">
    <div class="banner-obras">
        <div class="banner-obras-conteudo ">
            <div class="obras-main">
                <div class="obras-carrossel <?= $background_color?>">
                    <div class="obras-carrossel-conteudo">
                        <div class="bloco-imagem anime anime-right">
                            <div class="imagem-produto">
                                <img src="<?= $imagem ?>">
                            </div>
                        </div> 
                        <div class="bloco-textos anime anime-right">
                            <h1 class="<?= $color ?>"><?= $titulo ?></h1>
                            <p class="<?= $color ?>"><?= $descricao ?></p>
                            <a href="<?= get_home_url()?>/nossas-obras/obras/obras-de-marcenaria">
                                <button class="<?= $cor_botao ?>"><?= $texto_botao ?></button>
                            </a>
                        </div>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php 
    $imagem =  get_field('bloco_obras')['bloco_segundo_obra']['imagem_destaque_segundo']['url'];
    $titulo =  get_field('bloco_obras')['bloco_segundo_obra']['titulo_obra'];
    $descricao =  get_field('bloco_obras')['bloco_segundo_obra']['descricao_obras'];
    $background_color =  get_field('bloco_obras')['bloco_segundo_obra']['cor_fundo_background'];
    $color =  get_field('bloco_obras')['bloco_segundo_obra']['cor_textos'];       
    $texto_botao =  get_field('bloco_obras')['bloco_segundo_obra']['texto_botao'];   
    $cor_botao =  get_field('bloco_obras')['bloco_segundo_obra']['cor_botao'];
    
?> 

<div class="obra-segundo">
    <div class="banner-obras">
        <div class="banner-obras-conteudo ">
            <div class="obras-main">
                <div class="obras-carrossel <?= $background_color?>">
                    <div class="obras-carrossel-conteudo">
                        <div class="bloco-textos  anime anime-left">
                            <h1 class="<?= $color ?>"><?= $titulo ?></h1>
                            <p class="<?= $color ?>"><?= $descricao ?></p>
                            <a href="<?= get_home_url()?>/nossas-obras/obras/obras-de-ambientacao">
                                <button class="<?= $cor_botao ?>"><?= $texto_botao ?></button>
                            </a>
                        </div>
                        <div class="bloco-imagem  anime anime-left">
                            <div class="imagem-produto">
                                <img src="<?= $imagem ?>">
                            </div>
                        </div>         
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php 
        
    $imagem = get_field('bloco_obras')['bloco_terceiro_obra']['imagem_destaque']['url'];
    $titulo = get_field('bloco_obras')['bloco_terceiro_obra']['titulo_obra'];
    $descricao = get_field('bloco_obras')['bloco_terceiro_obra']['descricao_obras'];
    $background_color = get_field('bloco_obras')['bloco_terceiro_obra']['cor_fundo_background'];
    $color = get_field('bloco_obras')['bloco_terceiro_obra']['cor_textos'];       
    $texto_botao = get_field('bloco_obras')['bloco_terceiro_obra']['texto_botao'];   
    $cor_botao = get_field('bloco_obras')['bloco_terceiro_obra']['cor_botao'];
    
?> 

<div class="obra-terceira">
    <div class="banner-obras">
        <div class="banner-obras-conteudo ">
            <div class="obras-main">
                <div class="obras-carrossel <?= $background_color?>" >
                    <div class="obras-carrossel-conteudo">
                        <div class="bloco-textos anime anime-right">
                            <h1 class="<?= $color ?>"><?= $titulo ?></h1>
                            <p class="<?= $color ?>"><?= $descricao ?></p>
                            <a href="<?= get_home_url()?>/nossas-obras/obras/obras-de-decoracao">
                                <button class="<?= $cor_botao ?>"><?= $texto_botao ?></button>
                            </a>
                        </div>
                        <div class="bloco-imagem anime anime-right">
                            <div class="imagem-produto">
                                <img src="<?= $imagem ?>">
                            </div>
                        </div>         
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
</script>


<?php get_footer();?>