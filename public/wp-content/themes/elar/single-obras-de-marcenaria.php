<?php
    wp_enqueue_style('css_obras_de_decoracao_e_marcenaria', get_stylesheet_directory_uri().'/src/css/obras-de-decoracao-e-marcenaria.min.css', array(), null, false);
    get_header();

?>
<div class="banner-background camada-verde" style="background-image: url('<?= get_field('bloco_banner_obra', $post->ID)['banner_imagem_background']['url'] ?>')">
    <div class="banner-container-pai">
        <div class="banner-container-conteudo-pai">
            <div class="container-padrao2">
                <div class="banner-titulo">
                    <h1><?= get_field('bloco_inicial', $post->ID)['titulo_da_pagina'] ?></h1>
                    <p><?= get_field('bloco_inicial', $post->ID)['descricao_pagina'] ?></p>
                </div>
                <div class="banner-textos-pai">
                    <div class="container-texto">
                        <div class="container-bloco-borda"></div>
                        <div class="container-conteudo">
                            <div class="container-taxonomia">
                                <p>Marcernaria</p>
                            </div>
                            <h1><?= $post->post_title ?></h1>
                            <p><?= get_field('bloco_banner_obra', $post->ID)['descricao_obra'] ?></p>
                        </div>
                    </div>
                    <div class="container-imagem">
                        <img src="<?= get_field('bloco_banner_obra', $post->ID)['imagem_obra']['url'] ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-galeria-pai">
    <div class="container-padrao3">
        <h1>Galeria de Fotos</h1>
    </div>
    <div class="container-galeria">
        <?php
            $galeria = get_field('bloco_sobre', $post->ID)['galeria_imagens'];
            $contadorGaleria = count($galeria);
            for($i = 0; $i < $contadorGaleria; $i ++){
                $imagem = $galeria[$i];
        ?>
            <div class="container-imagem">
                    <img src="<?= $imagem['imagem_da_galeria']['url'] ?>">
            </div>
        <?php } ?>
    </div>
</div>


<?php
    $taxArg = wp_get_post_terms( $post->ID, array( 'categoria_marcenaria' ))[0]->slug;
    if(!empty($taxArg)){
        $taxQuery =  array(
            array(
                'taxonomy' => 'categoria_marcenaria',
                'field' => 'slug',
                'terms' => $taxArg
            )
        );

        $obrasSimilaresArgs = array(
            'post_type'  => 'obras-de-marcenaria',
            'orderby' => 'date',
            'order' => 'DESC',
            'parent' => '0',
            'post_status' => 'any',
            'hide_empty' => false,
            'numberposts' => 8,
            'tax_query' => $taxQuery,
            'post__not_in' => array (get_the_ID($post))
        ); 
        $obrasSimilares = get_posts($obrasSimilaresArgs);
        $contadorObrasSimilares = count($obrasSimilares);
?>
<div class="container-obras-similares-pai">
    <div class="container-padrao">
        <h1>Obras similares</h1>
            <?php
                if($contadorObrasSimilares > 0){
            ?>
        <div class="container-obras-similares">
            <?php
                    for($i = 0; $i < $contadorObrasSimilares; $i ++){
                        $obraSimilar = $obrasSimilares[$i];
            ?>
                <div class="container-obra-similar-pai">
                    <a href="/obras-de-marcenaria/<?= $obraSimilar->post_name ?>">
                        <div class="container-obra-similar">
                            <div class="container-texto">
                                <h1><?= $obraSimilar->post_title ?></h1>
                            </div>
                            <div class="container-imagem">
                                <img src="<?= get_field('bloco_banner_obra', $obraSimilar->ID)['imagem_obra']['url'] ?>">
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
        <?php }else{ ?>
            <div class="container-obra-similar-vazio">
                <p>Não há obras similares</p>
            </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
<?php

    $background = get_field('bloco_formulario_contato', 'geral')['imagem_background']['url'];
    $logo = get_field('bloco_formulario_contato', 'geral')['logo']['url'];
    $frase = get_field('bloco_formulario_contato', 'geral')['frase'];
    $titulo_formulario = get_field('bloco_formulario_contato', 'geral')['titulo_formulario'];
    $subtitulo_formulario = get_field('bloco_formulario_contato', 'geral')['subtitulo_formulario'];
    $texto_botao = get_field('bloco_formulario_contato', 'geral')['texto_botao'];

?>

<div class="formulario-obra camada-branca" style="background-image: url('<?= $background ?>')">
    <div class="container-padrao2" >
        <div class="informacoes-pai">
            <div class="informacoes-filho">
                <img src="<?= $logo ?>">
                <p><?= $frase ?></p>
            </div>
        </div>
        <div class="banner-formulario">
            <h1><?= $titulo_formulario ?></h1>
            <p><?= $subtitulo_formulario ?></p>
            <form data-hc-form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
                <input type="text" name="nome" required hc-mail-message placeholder="Digite seu nome">
                <input type="text" name="email" required hc-mail-message placeholder="Digite seu email de contato">
                <input type="text" name="assunto" required hc-mail-message" placeholder="Insira o assunto aqui">
                <textarea name="mensagem" required hc-mail-message placeholder="Digite aqui a sua mensagem…"></textarea>
                <button type="submit"><?= $texto_botao ?></button>
                <span data-hc-feedback></span>
            </form>
        </div>
    
    </div>
</div>
<script>

    jQuery(document).ready(function($){
        $('.container-galeria').slick({
            arrows: false,
            dots: true,
            infinite: true,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true,
            customPaging: function(slider, i) { 
                return '<button class="dot"></button>';
            }
        });
        $('.container-obras-similares').slick({
            arrows: false,
            dots: true,
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            customPaging: function(slider, i) { 
                return '<button class="dot"></button>';
            },
            responsive: [
                {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
                },
                {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
                },
                {
                breakpoint: 700,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
            ]
        });
    });

</script>

<?php
    get_footer();
?>