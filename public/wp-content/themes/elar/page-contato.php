<?php
    wp_enqueue_style('css_contato', get_stylesheet_directory_uri().'/src/css/contato.min.css', array(), null, false);
    get_header();
?>

<?php 

$background = get_field('bloco_contato')['imagem_background']['url'];
$titulo = get_field('bloco_contato')['titulo_banner'];
$texto = get_field('bloco_contato')['texto'];
$titulo_formulario = get_field('bloco_contato')['titulo_formulario'];

?>

<div class="banner-background camada-verde" style="background-image: url('<?= $background ?>')">
    <div class="banner-container-pai">
        <div class="banner-container-conteudo-pai">
            <div class="container-padrao2">

                <div class="banner-titulo-pai centralizar">
                    <div class="banner-titulo-filho">
                        <h1><?= $titulo ?></h1>
                    </div>
                </div>

                <div class="banner-textos-pai">

                    <div class="banner-textos-filho">
                        <div class="banner-descricao">
                            <p><?= $texto ?></p>
                            <div class="redes">
                                <div class="container-redes-pai">
                                    <?php
                                        $redes = get_field('redes_sociais', 'geral');
                                        $contadorRedes = count($redes);
                                        for($i =0; $i < $contadorRedes; $i++){
                                            $rede = $redes[$i];
                                    ?>

                                    <div class="container-rede">
                                        <a target="_blank" href="<?= $rede['link']?>"><img src="<?= $rede['icone']['url']?>"></a>
                                     </div>
                                        
                                    <?php } ?>
                                   
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="banner-textos-filho">
                        <div class="banner-informacoes">
                            <?php
                                $informacoes = get_field('informacoes_funcionamento', 'geral');
                                $contador = count($informacoes);
                                for($i =0; $i < $contador; $i++){
                                    $informacao = $informacoes[$i];
                            ?>

                            <div class="informacao-funcionamento">
                                <div class="titulo">
                                    <h1><?= $informacao['titulo'] ?></h1>
                                </div>
                                <div class="descricao">
                                    <p><?= $informacao['descricao'] ?></p>
                                </div>
                            </div>
                            <?php
                                }
                            ?>
                        </div>

                        <?php 
				
                            $email = get_field('contato', 'geral')['email'];
                            $telefone = get_field('contato', 'geral')['telefone'];

                        ?>

                        <div class="banner-contatos">
                            <div class="contato-cabecalho">
                                <p>e-mail</p>
                            </div>
                            <div class="contato-conteudo">
                                <img src="<?= get_stylesheet_directory_uri()?>/img/mail.svg">
                                <p><?= $email ?></p>
                            </div>

                            <div class="contato-cabecalho">
                                <p>telefone</p>
                            </div>
                            <div class="contato-conteudo">
                                <img src="<?= get_stylesheet_directory_uri()?>/img/phone.svg">
                                <p><?= $telefone ?></p>
                            </div>
                        </div>

                    </div>

                    <div class="banner-textos-filho">
                        <div class="banner-formulario">
                            <h1><?= $titulo_formulario ?></h1>
                            <form data-hc-form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
                                <input type="text" name="nome" required hc-mail-message placeholder="Digite seu nome">
                                <input type="text" name="email" required hc-mail-message placeholder="Digite seu email de contato">
                                <input type="text" name="assunto" required hc-mail-message placeholder="Insira o assunto aqui">  
                                <textarea name="mensagem" required hc-mail-message placeholder="Digite aqui a sua mensagem…"></textarea>
                                <button type="submit">Enviar</button>
                                <span data-hc-feedback></span> 
                            </form>
                        </div>                        
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="container-map-pai anime anime-fade">
    <div class="container-total">
        <div class="container-padrao">
            <?php
                 $enderecos = get_field('enderecos');
                 $contadorEnderecos = count($enderecos);
                 $latitudeSede = '';
                 $longitudeSede = '';
                 $marcadores = array();
                 for($i = 0; $i < $contadorEnderecos; $i++){
                     $endereco = $enderecos[$i];
                     $marcadores[$i]['nome'] = $endereco['titulo'];
                     $marcadores[$i]['lat'] = $endereco['latitude'];
                     $marcadores[$i]['lon'] = $endereco['longitude'];
                     $marcadores[$i]['conjunto'] = $i;
                     if($i == 0){
                         $latitudeSede = $endereco['latitude'];
                         $longitudeSede = $endereco['longitude'];
                         $nomeSede = $endereco['titulo'];
                     }
                 }
            ?>
            <div id="map"></div>
        </div>
    </div>
</div>

<script>
    var map;
    var marcadores = <?= json_encode($marcadores); ?>;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <?= $latitudeSede ?>, lng: <?= $longitudeSede ?>},
            zoom: 16
        });

        var sede = {
            scaledSize: new google.maps.Size(20, 20),
            position: { lat: <?= $latitudeSede; ?> , lon: <?= $longitudeSede; ?>, nome: '<?= $nomeSede; ?>' },
        }

        adicionarMarcadores(marcadores);
    }
    function adicionarMarcadores(marcadores){
        let count = marcadores.length;
        for(var i = 0; i < count; i++){
            var marcador = marcadores[i];
            var latitude = parseFloat(marcador.lat);
            var longitude = parseFloat(marcador.lon);
            var longitude = parseFloat(marcador.lon);
            var titulo = marcador.nome;
            var conjunto = marcador.conjunto;
            var marker = new google.maps.Marker({
                position: {lat: latitude, lng: longitude},
                title: titulo,
                map: map,
                id: conjunto,
                scaledSize: marcador.scaledSize,
            });
        }
    }
    function mapCenterController(id){
        map.setCenter(new google.maps.LatLng(marcadores[id].lat, marcadores[id].lon));
        map.setZoom(18);
        abrirDropDown();
    }
    jQuery(document).ready(function($){ 
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABUzRceZWnB7VWvjb3_K_GxyNGTK74ITM&callback=initMap" async defer></script>
<?php get_footer();?>