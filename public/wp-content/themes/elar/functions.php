<?php
//******* LINKS PARA SCRIPTS CSS E JS */

/******* Scripts padrões *******/
include __DIR__."/incs/links_base.php"; 

/******* Links de plugins *******/
include __DIR__."/incs/links_plugins.php"; 

//****** CUSTOM */

/******* Custom config *******/
include __DIR__."/incs/custom_config.php";

/******* Custom image size *******/
include __DIR__."/incs/custom_image_sizes.php";

/******* Custom URL *******/
include __DIR__."/incs/custom_url.php";

//****** INFORMAÇÕES GLOBAIS

//include __DIR__."/incs/custom_info.php";

//******* ACF OPTIONS */

/******* Configurações para o acf options ********/
include __DIR__."/incs/acf_options.php";

/******* Custom post types *******/
include __DIR__."/incs/custom_post_type.php";

/******* Custom taxonomy *******/
include __DIR__."/incs/custom_tax.php";


function be_ajax_load_more() {

    $postsCarregado = 0;
    $args = array(
        'numberposts'      => 12,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'posts_per_page' => 12,
        'paged' => $_POST['page'],
        'post_type'  => $_POST['catquery'],
        'tax_query' => $_POST['filtro']
    );
    $postsCarregado = get_posts($args);

    $containerResponse = array( );
    $countPosts = count($postsCarregado);
    if($countPosts == 0){
        echo 'false';
        die();
    }else{
        for($i = 0; $i < $countPosts; $i++){

            $titulo = $postsCarregado[$i]->post_title;  
            
            $descricao = get_field('bloco_banner_obra', $postsCarregado[$i])['descricao_obra'];

            if (strlen($descricao) > 100){
                $descricao = substr($descricao, 0, 97)."..."; 
            }else{
                $descricao = $descricao;
            }

            $link = get_home_url() . '/' . $postsCarregado[$i]->post_type . '/'. $postsCarregado[$i]->post_name;

            $imagem = get_field('bloco_banner_obra', $postsCarregado[$i])['banner_imagem_background']['url'];

            $postType = get_post_type_object($postsCarregado[$i]->post_type);
            $postTypeName = $postType->labels->singular_name;
            $PostTypeSlug = $postType->name;

            $imagemSeta = get_stylesheet_directory_uri().'/img/right-arrow.svg';
            array_push($containerResponse, array("titulo" => $titulo, "descricao" => $descricao, "link" => $link, "imagem" => $imagem, "tag" => $PostTypeSlug, "post_type_name" => $postTypeName, "imagem_seta" => $imagemSeta));
        }

        $postsJson = json_decode($postsCarregado, true);

        echo json_encode($containerResponse);
        die();
    }
}
add_action( 'wp_ajax_be_ajax_load_more', 'be_ajax_load_more' );
add_action( 'wp_ajax_nopriv_be_ajax_load_more', 'be_ajax_load_more' );

