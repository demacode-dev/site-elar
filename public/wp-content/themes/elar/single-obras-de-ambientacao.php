<?php
    wp_enqueue_style('css_contato', get_stylesheet_directory_uri().'/src/css/obras-de-ambientacao.min.css', array(), null, false);
    get_header();
?>

<?php

    $titulo = $post->post_title; 
    $background = get_field('bloco_banner_obra')['banner_imagem_background']['url'];
    $descricao = get_field('bloco_banner_obra')['descricao_obra'];
    $texto = get_field('bloco_ambientacao_inicial')['texto_obra'];


?>

<div class="banner-background camada-verde" style="background-image: url('<?= $background ?>')">
    <div class="banner-container-pai">
        <div class="banner-container-conteudo-pai">
            <div class="container-padrao2">

                <div class="banner-titulo-pai centralizar">
                        <div class="categoria centralizar">
                            <?php 

                                $categoriaPaiTerm = get_term_by('slug', 'ambiente', 'categoria');

                                $argsCategoriaAmbiente = array(
                                    'parent' => $categoriaPaiTerm->term_id
                                );

                                $categoriasAmbiente = wp_get_object_terms( $post->ID, 'categoria', $argsCategoriaAmbiente );

                                $cat_name = $categoriasAmbiente[0]->name;

                            ?>
                            <p><?= $cat_name  ?></p>
                        </div>
                    
                    <div class="banner-titulo-filho">
                        <div class="textos">
                            <h1><?= $titulo ?></h1>
                            <p><?= $descricao ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="obra-sobre">
    <div class="container-padrao">
        <div class="obra-conteudo">
            <div class="texto">
                <p><?= $texto ?></p>
            </div>

            <div class="filtros">
                <?php

                    $categoriasPais = get_terms(
                        array (
                            'taxonomy' => 'categoria',
                            'orderby' => 'name',
                            'order' => 'ASC',
                            'parent' => 0,
                            'hide_empty' => true,
                            'hierarchical' => true
                        )
                    );
                    $contadorCategoriasPais = count($categoriasPais);
                    for($i = 0; $i < $contadorCategoriasPais; $i++){
                        $categoriaPai = $categoriasPais[$i];

                        $argsCategoriaFilhas = array(
                            'parent' => $categoriaPai->term_id
                        );
                        $categoriasFilhas = wp_get_object_terms( $post->ID, 'categoria', $argsCategoriaFilhas );

                        $contadorCategoriasFilhas = count($categoriasFilhas);
                        if($contadorCategoriasFilhas > 0){
                ?>
                    <li>
                        <a><?=$categoriaPai->name?></a>
                        <ul>
                            <?php
                                for($j = 0; $j < $contadorCategoriasFilhas; $j++){
                                    $categoriaFilha = $categoriasFilhas[$j];
                            ?>
                                <li>
                                    <a><?=$categoriaFilha->name?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } } ?>
            </div>

        </div>
    </div>
</div>

<div class="container-galeria-pai">
    <div class="container-padrao3">
        <h1>Galeria de Fotos</h1>
    </div>
    <div class="container-galeria">
        <?php
            $galeria = get_field('bloco_ambientacao_inicial')['galeria_de_imagens'];
            $contadorGaleria = count($galeria);
            for($i = 0; $i < $contadorGaleria; $i ++){
                $imagem = $galeria[$i];
        ?>
            <div class="container-imagem">
                    <img src="<?= $imagem['imagem_da_obra']['url'] ?>">
            </div>
        <?php } ?>
    </div>
</div>

<div class="container-obras-similares-pai">
    <div class="container-padrao">
        <h1 class="titulo">Decorações dessa obra</h1>
        <div class="container-obras-similares">
            <?php

                $decoracoes = get_field('carrossel_decoracao');
                $contadorDecoracoes = count($decoracoes);

                for($i = 0; $i < $contadorDecoracoes; $i ++){
                    $decoracoes = get_field('carrossel_decoracao')[$i]['obra_de_decoracao'];
                    $imagem = get_field('bloco_banner_obra', $decoracoes)['imagem_obra']['url'];
                    
                    $nome_obra = get_the_title( $decoracoes );
                    $link = get_permalink( $decoracoes );
                 
            ?>
            <div class="container-obra-similar-pai">
                <a href="<?= $link ?>">
                    <div class="container-obra-similar card-verde">
                        <div class="container-texto">
                            <h1><?= $nome_obra ?></h1>
                        </div>
                        <div class="container-imagem">
                            <img src="<?= $imagem ?>">
                        </div>
                    </div>
                </a>
            </div>
            <?php }?> 
        </div>
    </div>
</div>

<div class="container-obras-similares-pai">
    <div class="container-padrao">
        <h1 class="titulo">Marcenaria dessa obra</h1>
        <div class="container-obras-similares carde-cinza">
            <?php

                $marcenarias = get_field('carrossel_marcenaria');
                $contadorMarcenaria = count($marcenarias);
                
                for($i = 0; $i < $contadorMarcenaria; $i ++){
                    $marcenaria = $marcenarias[$i]['obras_de_marcenaria'];
                    $imagem = get_field('bloco_banner_obra', $marcenaria )['imagem_obra']['url'];
                    $nome_obra = get_the_title( $marcenaria );
                    $link = get_permalink( $marcenaria );
                 
            ?>
            <div class="container-obra-similar-pai">
                <a href="<?= $link ?>">
                    <div class="container-obra-similar carde-cinza">
                        <div class="container-texto">
                            <h1><?= $nome_obra ?></h1>
                        </div>
                        <div class="container-imagem">
                            <img src="<?= $imagem ?>">
                        </div>
                    </div>
                </a>
            </div>
            <?php }?> 
        </div>
    </div>
</div>

<?php 

    $designers = get_field('bloco_informacoes_designer');
    $cargo = (get_field('bloco_sobre_designer', $designers)['cargo']);
    $nome = (get_field('bloco_sobre_designer', $designers)['nome_designer']);
    $foto = (get_field('bloco_sobre_designer', $designers)['imagem_designer']['url']);
    $comentario = get_field('bloco_texto_designer');

?>

<div class="designer">
    <div class="container-padrao">
        <div class="textos">
            <div class="cabecalho">
                <div class="texto-designer">
                    <h1><?= $nome ?></h1>
                    <p><?= $cargo ?></p>
                </div>
                <div class="redes-sociais">

                    <?php
                    
                        $redes = (get_field('bloco_sobre_designer', $designers)['redes_sociais']);
                        $count = is_countable($redes) ? count($redes) : 0;

                        for ($i=0; $i < $count; $i++): 
                            $icone = $redes[$i]['icone_rede']['url']; 
                            $link = $redes[$i]['link_rede'];
                    
                    ?>

                    <a href="<?= $link ?>" target="_blank"><img src="<?= $icone ?>"></a>

                    <?php 
                    
                        endfor
                    ?>

                </div>
            </div>
            <div class="texto-designer">
                <p><?= $comentario ?></p>
            </div>
        </div>
        <div class="imagem">
            <img src="<?= $foto ?>">
        </div>
    </div>
</div>

<?php

    $background = get_field('bloco_formulario_contato', 'geral')['imagem_background']['url'];
    $logo = get_field('bloco_formulario_contato', 'geral')['logo']['url'];
    $frase = get_field('bloco_formulario_contato', 'geral')['frase'];
    $titulo_formulario = get_field('bloco_formulario_contato', 'geral')['titulo_formulario'];
    $subtitulo_formulario = get_field('bloco_formulario_contato', 'geral')['subtitulo_formulario'];
    $texto_botao = get_field('bloco_formulario_contato', 'geral')['texto_botao'];

?>

<div class="formulario-obra camada-branca" style="background-image: url('<?= $background ?>')">
    <div class="container-padrao2" >
        <div class="informacoes-pai">
            <div class="informacoes-filho">
                <img src="<?= $logo ?>">
                <p><?= $frase ?></p>
            </div>
        </div>
        <div class="banner-formulario">
            <h1><?= $titulo_formulario ?></h1>
            <p><?= $subtitulo_formulario ?></p>
            <form data-hc-form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
                <input type="text" name="nome" required hc-mail-message placeholder="Digite seu nome">
                <input type="text" name="email" required hc-mail-message placeholder="Digite seu email de contato">
                <input type="text" name="assunto" required hc-mail-message placeholder="Insira o assunto aqui">
                <textarea type="text" name="mensagem" required hc-mail-message placeholder="Digite aqui a sua mensagem…"></textarea>
                <button><?= $texto_botao ?></button>
                <span data-hc-feedback></span>
            </form>
        </div>
    
    </div>
</div>

<div class="obras-similares">
    <div class="container-padrao">
        <div class="cabecalho">
            <h1>OBRAS SIMILARES</h1>
        </div>
        <div class="container-cards-obras centralizar">

             <?php

                $args = new WP_Query(array(
                    'post_type' => 'obras-de-ambientacao',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'numberposts' => 4,
                    'posts_per_page' => 4,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'categoria',
                            'field' => 'slug',
                            'terms' => $cat_name
                        )
                    ),
                    'hide_empty' => false,
                    'post__not_in' => array (get_the_ID($post))                 
                ));

                $posts = $args->posts;
                $count = is_countable($posts) ? count($posts) : 0;
                
                if($count > 0){
                    for ( $i=0; $i < $count; $i++ ) : 

                        $titulo = $posts[$i]->post_title;                    
                        $link_post = get_permalink( $posts[$i]->ID );                   
                        $id_obra = $posts[$i]->ID;
                        $background = get_field( 'bloco_banner_obra', $id_obra )['banner_imagem_background']['url'];
                        $breve_texto = get_field( 'bloco_banner_obra', $id_obra )['descricao_obra'];
            ?> 
                    <div class="card-obra">
                        <div class="imagem camada-preta" style="background-image: url('<?= $background ?>')">
                            <a href="<?= $link_post ?>">
                                <div class="botao centralizar">
                                    <img src="<?= get_stylesheet_directory_uri()?>/img/right-arrow.svg">
                                </div>
                            </a>
                            <div class="card-conteudo">                                
                                <div class="etiqueta etiqueta-cinza centralizar">
                                    <p>Ambientação</p>
                                </div>

                                <div class="textos">
                                    <h1><?= $titulo ?></h1>
                                    <p><?= $breve_texto ?></p>
                                </div>

                            </div>
                        </div>

                    </div>

            <?php  
                    wp_reset_postdata();
                    endfor;
                }else{
            ?>
                <div class="container-obra-similar-vazio">
                    <p>Não há obras similares</p>
                </div>
            <?php
                }
            ?>
        </div>
    </div>
</div>

<script>

    jQuery(document).ready(function($){

        $('.carrossel-imagens').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            infinite: true,
            fade: false,
        });

    });

    jQuery(document).ready(function($){
        $('.container-galeria').slick({
            arrows: false,
            dots: true,
            infinite: true,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true,
            customPaging: function(slider, i) { 
                return '<button class="dot"></button>';
            }
        });
        $('.container-obras-similares').slick({
            arrows: false,
            dots: true,
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            customPaging: function(slider, i) { 
                return '<button class="dot"></button>';
            },
            responsive: [
                {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
                },
                {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
                },
                {
                breakpoint: 700,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
            ]
        });
    });

</script>

<?php
    get_footer();
?>