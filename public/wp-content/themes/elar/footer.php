<?php
	wp_enqueue_style('css_footer', get_stylesheet_directory_uri().'/src/css/footer.min.css', array(), null, false);

	$background = get_field('bloco_novidades', 'footer')['imagem_background']['url'];
	$titulo = get_field('bloco_novidades', 'footer')['titulo'];
	$descricao = get_field('bloco_novidades', 'footer')['descricao'];
	$texto_botao = get_field('bloco_novidades','footer')['texto_botao'];

?>

<div class="receba-novidades centralizar camada-branca-esquerda" style="background-image: url('<?= $background ?>'); ">
    <div class="container-padrao">  

        <div class="textos anime anime-right">
            <h1><?= $titulo ?></h1>
            <p><?= $descricao ?></p>
        </div>

        <form method="POST" action="<?php echo admin_url('admin-ajax.php');?>" data-hibrido-subscribers-form class="anime anime-right">
            <input id="email" type="text" name="email" placeholder="Insira seu email aqui…">
			<button type="submit"><?= $texto_botao ?></button>
			<p data-hibrido-subscribers-response></p>
        </form>

    </div>
</div>

<footer>
	<div class="container-padrao">
		<div class="container-filho">

			<div class="container-conteudo-primeiro">

				<div class="logo">
					<img src="<?= get_field('logo', 'header')['url']?>">
				</div> 

				<div class="informacoes">

					<?php
						$informacoes = get_field('informacoes_funcionamento', 'geral');
						$contador = count($informacoes);
						for($i =0; $i < $contador; $i++){
							$informacao = $informacoes[$i];
					?>
				
					<div class="informacao-funcionamento">
						<div class="titulo">
							<h1><?= $informacao['titulo'] ?></h1>
						</div>
						<div class="descricao">
							<p><?= $informacao['descricao'] ?></p>
						</div>
					</div>
					<?php
						}
					?>
				</div>
				
				<?php 
				
				$email = get_field('contato', 'geral')['email'];
				$telefone = get_field('contato', 'geral')['telefone'];

				?>

				<div class="contatos">
					<div class="contatos-conteudo">
						<div class="contato-cabecalho">
							<p>e-mail</p>
						</div>
						<div class="contato-conteudo">
							<img src="<?= get_stylesheet_directory_uri()?>/img/mail.svg">
							<p><?= $email ?></p>
						</div>
					</div>
					<div class="contatos-conteudo">
						<div class="contato-cabecalho">
							<p>telefone</p>
						</div>
						<div class="contato-conteudo">
							<img src="<?= get_stylesheet_directory_uri()?>/img/phone.svg">
							<p><?= $telefone ?></p>
						</div>
					</div>
				</div>
				

			</div> 

			<div class="container-conteudo-segundo">
				<div class="cabecalho">
					<ul>
						<?php 
							$itens_menu = get_field('itens_menu', 'header');
							$count = is_countable($itens_menu) ? count($itens_menu) : 0;

							for ($i=0; $i < $count; $i++) : 
								$slug = $itens_menu[$i]['slug'];
								$label = $itens_menu[$i]['nome'];
						
						?>

						<li>
							<a href="/<?= $slug; ?>">
								<?= $label; ?>
							</a>
						</li>

						<?php 
							endfor 
						?>

					</ul>

				</div>
				<div class="container-redes-pai">
					<div class="container-redes">
						<?php
							$redes = get_field('redes_sociais', 'geral');
							$contadorRedes = count($redes);
							for($i =0; $i < $contadorRedes; $i++){
								$rede = $redes[$i];
						?>
						<div class="container-rede">
							<a target="_blank" href="<?= $rede['link']?>">
								<img src="<?= $rede['icone']['url']?>">
								<p><?= $rede['nome']?></p>
							</a>
						</div>
						
						<?php } ?>
					</div>
				</div>
				<div class="botao">
					<a class="scroll-top" >
						<div class="botao-conteudo centralizar">
							<img src="<?= get_stylesheet_directory_uri()?>/img/next-arrow.png">
						</div>
					</a>
				</div>

			</div>
		</div>

		<?php 

			$texto = get_field('texto_ultimo_rodape', 'footer');
			
		?>

		<div class="container-ultimo centralizar">
			<div class="texto">
				<p><?= $texto ?></p>
			</div>
		</div>

	</div>

</footer>

<script type="text/javascript" src="<?= get_template_directory_uri() ?>/js/plugins/slick/slick.min.js"></script>

<script>

	$(document).ready(function(){
		$(".scroll-top").click(function() {
			$("html, body").animate({ 
				scrollTop: 0 
			}, "slow");
			return false;
		});
	});

</script>

<?php wp_footer();?>


</body>
</html>