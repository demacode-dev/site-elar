<?php
    wp_enqueue_style('css_nossas_obras', get_stylesheet_directory_uri().'/src/css/nossas-obras.min.css', array(), null, false);
    get_header();

    $categoriaObras = get_query_var('categoriaobras', '');
    
?>
<div class="banner-background camada-verde" style="background-image: url('<?= get_field('bloco_banner')['imagem_background']['url'] ?>')">
    <div class="banner-container-pai">
        <div class="banner-container-conteudo-pai">
            <div class="container-padrao2">
                <div class="banner-titulo">
                    <h1><?= get_field('bloco_banner')['titulo'] ?></h1>
                    <p><?= get_field('bloco_banner')['texto'] ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-filtro-pai">
    <div class="container-padrao">
        <div class="container-filtro-obras-pai">
            <h1>Selecione o tipo de obra que deseja:</h1>
            <div class="container-filtro-obras">
                <div class="container-filtro-obra marcenaria <?= $categoriaObras == 'obras-de-marcenaria' ? 'ativo' : ''?>">
                    <a href="<?=get_home_url()?>/nossas-obras/obras/obras-de-marcenaria">
                        <div class="container-texto">
                            <h1><?= get_field('bloco_filtro')['marcenaria']['titulo']?></h1>
                            <p>VER AS OBRAS <img src="<?= get_stylesheet_directory_uri()?>/img/right-arrow-cinza.svg"></p>
                        </div>
                        <div class="container-imagem">
                            <img src="<?= get_field('bloco_filtro')['marcenaria']['imagem']['url'] ?>">
                        </div>
                    </a>
                </div>
                <div class="container-filtro-obra ambientacao <?= $categoriaObras == 'obras-de-ambientacao' ? 'ativo' : ''?> anime anime-left" style="transition-delay: .1s">
                    <a href="<?=get_home_url()?>/nossas-obras/obras/obras-de-ambientacao">
                        <div class="container-texto">
                            <h1><?= get_field('bloco_filtro')['ambientacao']['titulo']?></h1>
                            <p>VER AS OBRAS <img src="<?= get_stylesheet_directory_uri()?>/img/right-arrow-cinza.svg"></p>
                        </div>
                        <div class="container-imagem">
                            <img src="<?= get_field('bloco_filtro')['ambientacao']['imagem']['url'] ?>">
                        </div>
                    </a>
                </div>
                <div class="container-filtro-obra decoracao <?= $categoriaObras == 'obras-de-decoracao' ? 'ativo' : ''?> anime anime-left" style="transition-delay: .2s">
                    <a href="<?=get_home_url()?>/nossas-obras/obras/obras-de-decoracao">
                        <div class="container-texto">
                            <h1><?= get_field('bloco_filtro')['decoracao']['titulo']?></h1>
                            <p>VER AS OBRAS <img src="<?= get_stylesheet_directory_uri()?>/img/right-arrow-cinza.svg"></p>
                        </div>
                        <div class="container-imagem">
                            <img src="<?= get_field('bloco_filtro')['decoracao']['imagem']['url'] ?>">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-filtro-categoria-pai">
    <div class="container-padrao">
        <h1 class="anime anime-fade">Filtra obras:</h1>
        <div class="container-filtro-categoria">
            <?php 
                $argsCategoriasPai = array(
                    'post_type'  => array('obras-de-marcenaria', 'obras-de-ambientacao', 'obras-de-decoracao'),
                    'taxonomy' => 'categoria',
                    'parent' => '0',
                    'hide_empty' => false,
                    'orderby'	=> 'name',
                    'order' => "ASC",
                );  
                $categoriasPai = get_terms($argsCategoriasPai);

                $contadorCategoriasPai = count($categoriasPai);

                $arrayCategoriasQuery = array();
                for($i = 0; $i < $contadorCategoriasPai; $i++){
                    $categoriaPai = $categoriasPai[$i];
            ?> 
                <div class="container-seletor-pai-titulo anime anime-right" style="transition-delay: .<?=$i?>s">
                    <div class="container-seletor-pai">
                        <div class="container-seletor">
                            <div class="container-selected" onclick="abrirDropDown('<?=$categoriaPai->slug?>')">
                                <p id="texto-selected"><?=$categoriaPai->name?></p>
                                <div class="container-seta">
                                    <img id="seta-<?=$categoriaPai->slug?>" src="<?php echo get_template_directory_uri()?>/img/chevron-down-solid.svg">
                                </div>
                            </div>
                            <div class="container-options-pai" id="container-options-<?=$categoriaPai->slug?>">
                                <div class="container-options">
                                    <?php
                                        $argsCategoriasFilhas = array( 
                                            'post_type'  => 'produtos',
                                            'parent' => $categoriaPai->term_id,
                                            'post_status' => 'publish',
                                            'orderby'	=> 'name',
                                            'taxonomy' => 'categoria',
                                            'hide_empty' => false
                                        );
                                        
                                        $categoriasFilhas = get_terms($argsCategoriasFilhas);
                                        $contadorCategoriasFilhas = count($categoriasFilhas);

                                        $arrayFiltroCategoria = array();
                                        if(!empty($_GET[$categoriaPai->slug])){
                                            $arrayFiltroCategoria = explode(',', $_GET[$categoriaPai->slug]);
                                            $arraysFiltroCategoria = array($i => $arrayFiltroCategoria);
                                            $arrayCategoriasQuery = array_merge($arrayCategoriasQuery, $arraysFiltroCategoria);
                                        }

                                        for($j = 0; $j < $contadorCategoriasFilhas; $j++){
                                            $categoriaFilha = $categoriasFilhas[$j];
                                    ?>
                                        <label class="option" for="<?= $categoriaFilha->slug?>">
                                            <input <?= in_array($categoriaFilha->slug ,$arrayFiltroCategoria) ? 'checked' : ''?> type="checkbox" id="<?= $categoriaFilha->slug?>" name="<?= $categoriaFilha->slug?>" value="<?= $categoriaFilha->slug?>" class="check-box-<?=$categoriaPai->slug?>">
                                            <?= $categoriaFilha->name?>
                                        </label>
                                    <?php } ?>
                                </div>
                                <div class="container-buscar" onclick="filtroController('<?=$categoriaPai->slug?>')">
                                    <p>Buscar</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="container-obras-pai">
    <div class="container-padrao">
        <div class="container-obras">
            <?php
                if(empty($categoriaObras)){
                    $categoriasObrasQuery = array('obras-de-marcenaria', 'obras-de-ambientacao', 'obras-de-decoracao');
                }else{
                    $categoriasObrasQuery = array($categoriaObras);
                }

                $tax_query = array(
                    'relation' => 'AND'
                );

                $contadorArrayQuery = count($arrayCategoriasQuery);

                for($i = 0; $i < $contadorArrayQuery; $i++){
                    array_push($tax_query, array('taxonomy' => 'categoria', 'field'    => 'slug', 'terms' => $arrayCategoriasQuery[$i]));
                }

                $obrasArgs = new WP_Query(array(
                    'post_type' => $categoriasObrasQuery,
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'posts_per_page' => 12,
                    'hide_empty' => false,
                    'tax_query' => $tax_query
                ));

                $obras = $obrasArgs->posts;
                $contadorObras = is_countable($obras) ? count($obras) : 0;

                for ($i = 0; $i < $contadorObras; $i++){
                    $obra = $obras[$i];
                    $postType = get_post_type_object($obra->post_type);
                    $postTypeName = $postType->labels->singular_name;
            ?>
                <div class="container-obra-pai anime anime-fade">
                    <div class="container-obra camada-preta" style="background-image: url('<?= get_field( 'bloco_banner_obra', $obra->ID )['card_imagem_background']['url'] ?>')">
                        <div class="container-tag <?=$postType->name?>">
                            <p><?=$postTypeName?></p>
                        </div>
                        <div class="container-texto">
                            <h1><?=$obra->post_title?></h1>
                            <p>
                                <?php 
                                    $descricao = get_field('bloco_banner_obra', $obra->ID)['descricao_obra'];
                                    if( strlen($descricao) > 100){
                                        echo substr($descricao,0, 97)."..."; 
                                    }else{
                                        echo $descricao;
                                    }
                                ?>    
                            </p>
                        </div>
                        <a href="<?=get_home_url()?>/<?= $obra->post_type ?>/<?= $obra->post_name ?>">
                            <div class="container-botao">
                                <img src="<?= get_stylesheet_directory_uri()?>/img/right-arrow.svg">
                            </div>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="container-carregar-mais" onclick='carregarMais(<?=json_encode($categoriasObrasQuery)?>, <?=json_encode($tax_query)?> )'>
            <p class="carregar-mais-texto">Carregar mais</p>
        </div>
    </div>
</div>
<script>

    jQuery(document).ready(function($){
    });

</script>

<?php
    get_footer();
?>