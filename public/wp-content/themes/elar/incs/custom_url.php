<?php
add_action( 'init', function(){

    add_rewrite_tag('%categoriaobras%', '([0-9a-zA-Z\-]*)');

    add_rewrite_rule('^nossas-obras/obras/([0-9a-zA-Z\-]*)?','index.php?pagename=nossas-obras&categoriaobras=$matches[1]', 'top');

    flush_rewrite_rules();

});
