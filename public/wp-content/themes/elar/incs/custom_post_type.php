<?php
add_action( 'init', function(){
	
	//Obras de Decoração
	$labelsDecoracao = array(
        'name' => _x('Obras de Decoração', 'post type general name'),
        'singular_name' => _x('Decoração', 'post type singular name'),
        'add_new' => _x('Adicionar Nova', 'Novo item'),
        'add_new_item' => __('Novo Item'),
        'edit_item' => __('Editar Item'),
        'new_item' => __('Novo Item'),
        'view_item' => __('Ver Item'),
        'search_items' => __('Procurar Itens'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Obras de Decoração'
    );

	$argsDecoracao = array(
		'labels' => $labelsDecoracao,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'menu_position' => null,
		'taxonomies' => array(),
		'supports' => array('title'),
		'menu_icon' => 'dashicons-admin-customizer',
	);
	register_post_type( 'obras-de-decoracao' , $argsDecoracao ); 

	//Obras de Marcenaria
	$labelsMarcenaria = array(
        'name' => _x('Obras de Marcenaria', 'post type general name'),
        'singular_name' => _x('Marcenaria', 'post type singular name'),
        'add_new' => _x('Adicionar Nova', 'Novo item'),
        'add_new_item' => __('Novo Item'),
        'edit_item' => __('Editar Item'),
        'new_item' => __('Novo Item'),
        'view_item' => __('Ver Item'),
        'search_items' => __('Procurar Itens'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Obras de Marcenaria'
    );

	$argsMarcenaria = array(
		'labels' => $labelsMarcenaria,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'menu_position' => null,
		'taxonomies' => array(''),
		'supports' => array('title'),
		'menu_icon' => 'dashicons-hammer',
	);
	register_post_type( 'obras-de-marcenaria' , $argsMarcenaria );

	//Obras de Ambientação
	$labelsAmbientacao = array(
        'name' => _x('Obras de Ambientação', 'post type general name'),
        'singular_name' => _x('Ambientação', 'post type singular name'),
        'add_new' => _x('Adicionar Nova', 'Novo item'),
        'add_new_item' => __('Novo Item'),
        'edit_item' => __('Editar Item'),
        'new_item' => __('Novo Item'),
        'view_item' => __('Ver Item'),
        'search_items' => __('Procurar Itens'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Obras de Ambientação'
    );

	$argsAmbientacao = array(
		'labels' => $labelsAmbientacao,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'menu_position' => null,
		'taxonomies' => array(''),
		'supports' => array('title'),
		'menu_icon' => 'dashicons-admin-multisite',
	);
	register_post_type( 'obras-de-ambientacao' , $argsAmbientacao ); 

	//Obras de Designers
	$labelsDesigners = array(
        'name' => _x('Designers', 'post type general name'),
        'singular_name' => _x('Designers', 'post type singular name'),
        'add_new' => _x('Adicionar Novo', 'Novo item'),
        'add_new_item' => __('Novo Item'),
        'edit_item' => __('Editar Item'),
        'new_item' => __('Novo Item'),
        'view_item' => __('Ver Item'),
        'search_items' => __('Procurar Itens'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Designers'
    );

	$argsDesigners = array(
		'labels' => $labelsDesigners,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'menu_position' => null,
		'taxonomies' => array(''),
		'supports' => array('title'),
		'menu_icon' => 'dashicons-groups',
	);
	register_post_type( 'designers' , $argsDesigners ); 

});