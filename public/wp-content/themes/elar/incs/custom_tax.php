<?php

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it topics for your posts
 
function create_topics_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
    $labels = array(
        'name' => _x( 'Tipo de Categorias', 'taxonomy general name' ),
        'singular_name' => _x( 'Categoria', 'taxonomy singular name' ),
        'search_items' =>  __( 'Buscar Categorias' ),
        'all_items' => __( 'Todos as Categorias' ),
        'parent_item' => __( 'Categoria Pai' ),
        'parent_item_colon' => __( 'Categoria Pai:' ),
        'edit_item' => __( 'Editar Categoria' ), 
        'update_item' => __( 'Atualizar Categoria' ),
        'add_new_item' => __( 'Adicionar Nova Categoria' ),
        'new_item_name' => __( 'Adicionar Nova Categoria' ),
        'menu_name' => __( 'Categorias' ),
    );    
    
    // Now register the taxonomy
    
    register_taxonomy(
        'categoria',
        array(
            'obras-de-decoracao',
            'obras-de-marcenaria',
            'obras-de-ambientacao'
        ),
        array(
            'rewrite' => false,
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true
        )
    );
    
    register_taxonomy(
        'categoria_decoracao',
        'obras-de-decoracao',
        array(
            'label' => __( 'Categorias de Decoração' ),
            'rewrite' => false,
            'hierarchical' => true
        )
    );

    register_taxonomy(
        'categoria_marcenaria',
        'obras-de-marcenaria',
        array(
            'label' => __( 'Categorias de Marcenaria' ),
            'rewrite' => false,
            'hierarchical' => true
        )
    );

}

